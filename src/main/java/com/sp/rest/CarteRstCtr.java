package com.sp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Carte;
import com.sp.service.CarteService;

@RestController
public class CarteRstCtr {
	
	@Autowired
	private CarteService cService;
	
	
	@RequestMapping("/cartes")
	public String sayHello() {
		return "Listes des cartes !!!";
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/cartes/{id1}")
	public String getMsg(@PathVariable String id1) {
		int id=Integer.valueOf(id1);
		Carte carteAafficher = cService.getCarte(id);
		return "La carte appelée: "+carteAafficher.toString();
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/cartes")
	public void addCartes(@RequestBody Carte carte) {
		cService.addCarte(carte);
		System.out.println(carte);
	}

	@RequestMapping(method=RequestMethod.GET,value="/start")
	public String addCartesMultiple() {
		Carte carte = new Carte("DC Comic","https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg","BATMAN","Bruce Wayne, alias Batman, est un héros de fiction appartenant à l'univers de DC Comics. Créé par le dessinateur Bob Kane et le scénariste Bill Finger, il apparaît pour la première fois dans le comic book Detective Comics no 27 (date de couverture : mai 1939 mais la date réelle de parution est le 30 mars 1939) sous le nom de The Bat-Man. Bien que ce soit le succès de Superman qui ait amené sa création, il se détache de ce modèle puisqu'il n'a aucun pouvoir surhumain. Batman n'est qu'un simple humain qui a décidé de lutter contre le crime après avoir vu ses parents se faire abattre par un voleur dans une ruelle de Gotham City, la ville où se déroulent la plupart de ses aventures. Malgré sa réputation de héros solitaire, il sait s'entourer d'alliés, comme Robin, son majordome Alfred Pennyworth ou encore le commissaire de police James Gordon. ",50,80,170,80,100);
		cService.addCarte(carte);
		carte = new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("CPE","pasdimage","Green Lantern","Le film est pas fou",10,400,70,80,10);
		cService.addCarte(carte);
		carte = new Carte("OUO","pasdimage","ouais ouais ouais","un deux",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Marvel","pasdimage","test","je te test",50,800,170,80,10);
		cService.addCarte(carte);
		//System.out.println(carte);
		return "Ajout des cartes";
	}
}
